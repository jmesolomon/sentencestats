This coding exercise is part of my application for the awesome Intergen dev team.

To open the solution, please open the 
		IntergenExercise.sln with VS2015 (preferably)

The solution has two project files: {Console and MVC app} .. (It would have been nice if I had a go with UniversalApp, was good time to learn it..)
There are nothing fancy about the two apps. 
For the MVC app, I used AngularJS for the front-end functionalities. It has a little more capability as the user can specify the paragraph. 
For the console, nothing fancy, a straight-forward console.writeline() to display the results. Would have done the console.ReadLine for user input, but...

The main class that handles the logic is WordProcessor.cs
Included in the solution is a UnitTest which covers all public methods in WordProcessor.cs -> all tests are passing yeheey!

Structure:
	IntergenExcercise - MVC app
	IntergenExcerciseConsole - console
	WordProcessor - Class library
	WordProcessorTests - Unit test. on my work machine I had nCrunch

Please take note: 	I have used the Example Text provided by Intergen.
					I have tried to copy the result's syntax as much as possible.

After drinking some cups of coffee, now I can finally sleep :)

