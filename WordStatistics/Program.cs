﻿using System;
using System.Linq;
using WordProcessors;

namespace WordStatistics
{
    class Program
    {
        const string paragraph = @"Intergen is New Zealand's most experienced provider of Microsoft based business solutions.
                                        We focus on delivering business value in our solutions and work closely with Microsoft to ensure we have the best possible understanding of their technologies and directions. 
                                        Intergen is a Microsoft Gold Certified Partner with this status recognising us as an “elite business partner” for 
                                        implementing solutions based on our capabilities and experience with Microsoft products.";


        static void Main(string[] args)
        {
            var wordSta = new WordProcessor();

                //todo the userinput=console.ReadLine functionality
                //will work for now. 
                var sentenceWithTheMostWord = wordSta.SentenceWithTheMostWords(paragraph).First();
                var thirdLongestWord = wordSta.FindTheThirdLongestWord(paragraph);

                Console.WriteLine("Number of sentences: {0}", wordSta.CountTheSentence(paragraph));
                Console.WriteLine("Number of words: {0}", wordSta.WordCount(paragraph));
                Console.WriteLine("Sentence with the most words:{0} Sentence:\"{1}\"...", WordProcessor.Formatter(sentenceWithTheMostWord.Key), sentenceWithTheMostWord.Value);
                Console.WriteLine("The most used word is '{0}'", wordSta.MostUsedWord(paragraph));
                if (thirdLongestWord.Count == 1)
                {
                    Console.WriteLine("3rd longest word is '{0}'", thirdLongestWord.ElementAt(0));
                }
                else
                {
                    Console.Write("3rd longest words:");
                    thirdLongestWord.ForEach(i => Console.Write("\'{0}\' ", i));
                    Console.Write("and ");
                    Console.Write("are {0} characters long", thirdLongestWord[0].Length);
                }

            Console.ReadLine();
        }
    }
}
