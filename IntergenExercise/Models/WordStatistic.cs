﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntergenExercise.Models
{
    public class WordStatistic
    {

        //int number of sentence
        //int number of words
        //SentenceWithMostWords List<KeyValuePair<int, id>>
        //string mostusedword
        //list of thirdLongestWords
        public int NumberOfSentence { get; set; }
        public int NumberOfWords { get; set; }
        public string MostUsedWord { get; set; }
        public List<KeyValuePair<string, string>> SentenceWithMostWords { get; set; }
        public List<string> ThirdLongestWords { get; set; }


    }
}