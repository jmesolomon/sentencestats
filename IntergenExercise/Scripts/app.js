﻿//
//since this is a simple app, I just put all in one jsfile.
//in bigger app, I would have seperated and grouped 'em into different directories by components :)
//
var IntergenApp = angular.module('app', []);

IntergenApp.controller('WordProcessorController', function ($scope, WordStatService) {

    $scope.models = {
        title: 'Intergen Coding Exercise',
        paragraph: 'Intergen is New Zealand\'s most experienced provider of Microsoft based business solutions. We focus on delivering business value in our solutions and work closely with Microsoft to ensure we have the best possible understanding of their technologies and directions. \n \nIntergen is a Microsoft Gold Certified Partner with this status recognizing us as an \"elite business partner"\ for implementing solutions based on our capabilities and experience with Microsoft products.'
  
    };

    $scope.getWordStats = function (paragraph) {
        WordStatService.getWordStat(paragraph)
            .success(function (stats) {
                console.log('calling stats'); //debug
                $scope.stats = stats;
                console.log(JSON.stringify(stats)); //debug
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log(JSON.stringify(error));
                console.log($scope.status);

            });
    }

    $scope.clearAll = function ()
    {
        $scope.models.paragraph = '';
        $scope.stats = {}; //set to empty
    }

    $scope.clearStat = function ()
    {
        $scope.stats = {}; //set to empty
    }
});

IntergenApp.factory('WordStatService', ['$http', function ($http) {
    //
    var WordStatService = {};
    WordStatService.getWordStat = function (paragraph) {
        return $http({
            url: 'Home/GetWordStatistics',
            params: {paragraph: paragraph}
        });
    };
    return WordStatService;

}]);