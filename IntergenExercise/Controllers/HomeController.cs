﻿using IntergenExercise.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WordProcessors;

namespace IntergenExercise.Controllers
{
    public class HomeController : Controller
    {
        private WordProcessor WordProcessor; //just property injecting WordProcessor not using any DIC for simplicity

        public ActionResult Index()
        {
            return View();
        }


        /// Could have written a Web API to return JSON
        /// BUT
        /// just sticking with the simplest option
        /// of creating an action which returns JSON data
        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public JsonResult GetWordStatistics(string paragraph)
        {
            WordProcessor = new WordProcessor();

            var sentenceWithTheMostWord = WordProcessor.SentenceWithTheMostWords(paragraph).First();

            var thirdLongestWord = WordProcessor.FindTheThirdLongestWord(paragraph);

            var wordStat = new WordStatistic()
            {
                NumberOfSentence = WordProcessor.CountTheSentence(paragraph),
                NumberOfWords = WordProcessor.WordCount(paragraph),
                MostUsedWord = WordProcessor.MostUsedWord(paragraph),
                SentenceWithMostWords = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string> (WordProcessor.Formatter(sentenceWithTheMostWord.Key), sentenceWithTheMostWord.Value)
                },
                ThirdLongestWords = thirdLongestWord
            };

            return Json(wordStat, JsonRequestBehavior.AllowGet);
        }
    }
}