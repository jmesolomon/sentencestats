﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IntergenExercise.Startup))]
namespace IntergenExercise
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
